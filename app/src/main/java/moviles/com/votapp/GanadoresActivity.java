package moviles.com.votapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class GanadoresActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganadores);

        Button btnAtrasGanadores = findViewById(R.id.btnAtrasGanadores);
        btnAtrasGanadores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginView = new Intent(GanadoresActivity.this,PrincipalActivity.class);
                startActivity(loginView);
            }
        });
    }
}
