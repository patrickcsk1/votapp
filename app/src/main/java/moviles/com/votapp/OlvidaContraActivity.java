package moviles.com.votapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class OlvidaContraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_olvida_contra);

        Button btnAtrasRecContra = findViewById(R.id.btnAtrasRecContra);
        btnAtrasRecContra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginView = new Intent(OlvidaContraActivity.this,LoginActivity.class);
                startActivity(loginView);
            }
        });

        Button btnEnviarRecContra = findViewById(R.id.btnEnviarRecContra);
        btnEnviarRecContra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginView = new Intent(OlvidaContraActivity.this,LoginActivity.class);
                startActivity(loginView);
            }
        });
    }
}
