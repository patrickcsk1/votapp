package moviles.com.votapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NuevoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo);

        Button btnAtrasRegUsuario = findViewById(R.id.btnAtrasRegUsuario);
        btnAtrasRegUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginView = new Intent(NuevoActivity.this,LoginActivity.class);
                startActivity(loginView);
            }
        });

        Button btnRegistrarRegUsuario = findViewById(R.id.btnRegistrarRegUsuario);
        btnRegistrarRegUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginView = new Intent(NuevoActivity.this,LoginActivity.class);
                startActivity(loginView);
            }
        });
    }
}
