package moviles.com.votapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TextView txtOlvideContraIni = findViewById(R.id.txtOlvideContraIni);
        txtOlvideContraIni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent olvidaContraView = new Intent(LoginActivity.this,OlvidaContraActivity.class);
                startActivity(olvidaContraView);
            }
        });

        TextView txtNuevoUsuarioIni = findViewById(R.id.txtNuevoUsuarioIni);
        txtNuevoUsuarioIni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nuevoView = new Intent(LoginActivity.this,NuevoActivity.class);
                startActivity(nuevoView);
            }
        });
    }
}
